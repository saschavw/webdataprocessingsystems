from sklearn.ensemble import RandomForestClassifier
from pandas import read_csv, concat

import pickle
import os
import sys

def save_model(model, path):
    with open(path, "wb") as f:
        pickle.dump(model, f)

def fit_model(x, y):
    clf = RandomForestClassifier(n_estimators=100, max_depth=4)
    clf.fit(x, y)
    return clf

def get_xy(df):
    x = df.drop(['subject', 'predicate', 'object', 'target'], axis=1).values
    y = df['target'].values
    return x, y

def balance_data(df):
    true_relations = df.loc[df.target == 1].copy()
    false_relations = df.loc[df.target == 0].copy()

    df_balanced = (
        concat([
            true_relations, 
            false_relations.sample(len(true_relations)).copy()
        ])
        .sample(frac=1)
        .reset_index(drop=True)
        .copy()
    )
    return df_balanced

if __name__ == '__main__':
    try:
        _, INPUT, OUTPUT = sys.argv
    except Exception as e:
        print('Usage: python3.5 RE_train_model.py INPUT OUTPUT')
        sys.exit(0)

    df = read_csv(INPUT)
    df = balance_data(df)   
    x, y = get_xy(df)
    model = fit_model(x, y)
    save_model(model, OUTPUT)
