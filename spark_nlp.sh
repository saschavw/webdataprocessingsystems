#!/usr/bin/env bash

# Usage: bash spark_sample.sh [INFILE] [OUTFILE]
SCRIPT=${1:-"nlp_process.py"}
INFILE=${2:-"hdfs:///user/wdps1819/sample.warc.gz"}
OUTFILE=${3:-"predictions"}
DOMAIN=${4:-"http://rdf.freebase.com/ns/g.113pht7f07"}

hdfs dfs -rm -r $OUTFILE

PYSPARK_PYTHON=$(readlink -f $(which python)) ~/scratch/spark/bin/spark-submit \
--py-files dependencies.zip,nltk_data.zip \
--master yarn $SCRIPT $INFILE $OUTFILE $DOMAIN

hdfs dfs -cat $OUTFILE"/*" > $OUTFILE
