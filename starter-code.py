from bs4 import BeautifulSoup

import nltk
from nltk.corpus import stopwords
from nltk.tag import pos_tag
from nltk.tokenize import word_tokenize

import spacy
import en_core_web_sm

from pprint import pprint
import os
import io
import re
import requests
import json
import string

KEYNAME = "WARC-TREC-ID" # or WARC-RECORD-ID ??
KEYHTML = "Content-Type"

# initialization
# nlp = en_core_web_sm.load()
nlp = spacy.load("en_core_web_sm")
STOP_WORDS = set(stopwords.words('english'))

def find_labels(payload, es_domain, sparql_domain):
    blocks = payload.split('\n\n', maxsplit=2)

    key = None
    for line in blocks[0].splitlines():
        if line.startswith(KEYNAME):
            key = line.split(': ')[1]

    content = None
    if len(blocks) == 3:
        content = parse_html(blocks[2])

    # eliminate parsed html pages without content
    if content is None:
        return

    # remove blank lines
    content = remove_blank_lines(content)

    # SPACY finding entities and their labels (PERSON, ORG,..)
    doc = nlp(content)

    # NLTK
    #for chunk in nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(content))):
    #    if hasattr(chunk, 'label'):
    #        print(chunk.label(), ' '.join(c[0] for c in chunk))

    # getting sentences
    #sentences = [sent.string.strip() for sent in doc.sents]

    # display entity and label
    #pprint([(X.text, X.label_) for X in doc.ents])

    # processing entities with iterations
    for entity in doc.ents:
        if (entity.label_ == 'GPE' or entity.label_ == 'ORG' or entity.label_ == 'PERSON'):
            # Leaving entities only which are not very big, not starting with number, not contains specific simbols in the beggining
            if (len(entity.text) <= 20 and not entity.text[0].isdigit() and not entity.text == '’s' and entity.text[0] not in string.punctuation and special_symbols(entity.text)):
                query_elastic_search(entity.text, es_domain, sparql_domain, key)

    #labels = entity_recognition(payload)
    #for label, freebase_id in labels.items():
    #    if key:
    #        yield key, label, freebase_id

def special_symbols(mention):
    for c in mention:
        if c in string.punctuation:
            return False
    return True


def query_elastic_search(mention, es_domain, sparql_domain, key):
    url = 'http://%s/freebase/label/_search' % es_domain
    data = json.dumps({
        'query': {
            'match': {
                'label': {
                    'query': mention,
                    'operator': 'and',
                }
            }
        },
        'size': 1,
    })

    response = requests.get(url, data=data)
    if response:
        response = response.json()
        total = response.get('hits', {}).get('total')
        # Filtering by total results
        if (total < 3):
            return

        for hit in response.get('hits', {}).get('hits', []):
            freebase_id = hit.get('_source', {}).get('resource')

            score = hit.get('_score')
            # Filtering by score
            #if (score < 8,5):
            #    continue
            query_sparql(freebase_id, sparql_domain, mention, key)

def query_sparql(freebase, sparql_domain, mention, key):
    freebase_id = "<http://rdf.freebase.com/ns/" + freebase.replace('/m/', 'm.') +">"
    sameAs = " <http://www.w3.org/2002/07/owl#sameAs> "
    abstract = " <http://dbpedia.org/ontology/abstract> "
    sparqlurl = 'http://%s/sparql' % sparql_domain

    # queries examples
    limit = 5
    sparql_query = "select ?p where { " +  get_triple(freebase_id, "?p", "?o") + " } limit " + str(limit) + ""
    sparql_query = "select * where { " +  get_triple("?s", sameAs, freebase_id) + " } limit " + str(limit) + ""
    #sparql_query = "select * where { " + get_triple("?s", sameAs, freebase_id) " . " + get_triple("?o", abstract, "?abstract") + " . } limit " + str(limit) + ""
    #sparql_query = "select * where { " + get_triple("?s", sameAs, freebase_id) + " . " + get_triple("?s", sameAs, "?o") + " . } limit " + str(limit) + ""
    #sparql_query = "select distinct ?abstract where { " + get_triple("?s", sameAs, freebase_id) + " . " + get_triple("?s", sameAs, "?o") + " . " + get_triple("?o", abstract, "?abstract") + " .  } limit " + str(limit) + ""

    response = requests.post(sparqlurl, data={'print': True, 'query': sparql_query})
    if response:
        try:
            response = response.json()
            #print(json.dumps(response, indent=2)):q


            results_size = response.get('stats').get('nresults')
            if results_size != None and int(results_size) == limit:
                print(key + '\t' + mention + '\t' + freebase)
                return
                # todo: implement array which wouldcontain all results so writeoperation would be done just once (might improve speed)
                #freebase_label = hit.get('_source', {}).get('label')
                #freebase_id = hit.get('_source', {}).get('resource')
                #id_labels.setdefault(freebase_id, set()).add( freebase_label )

        except Exception as e:
            print(response)
            raise e

def get_triple(s, p, o):
    return s + p + o


def remove_blank_lines(content):
    #content = filter(lambda x: not re.match(r'^\s*$', x), content)
    #content = re.sub(' +',' ', content)
    content = os.linesep.join([s for s in content.splitlines() if s])
    content = " ".join(content.split())
    return content

def get_tokens(content):
    content = nltk.word_tokenize(content)
    return content

def remove_stop_words(content):
    content = [w for w in content if not w in STOP_WORDS]
    return content

def pos_tagging(content):
    content = nltk.pos_tag(content)
    return content

def parse_html(raw_html):
    soup = BeautifulSoup(raw_html, 'html.parser')

    # eliminate <script><script/>
    for s in soup.find_all('script'):
        s.extract()

    raw_text = soup.get_text()
    return raw_text

def entity_recognition(webpage):
    cheats = dict((line.split('\t',2) for line in open('./data/sample-labels-cheat.txt').read().splitlines()))

    entities = {}
    for label, freebase_id in cheats.items():
        if label in webpage:
            entities[label] = freebase_id
    return entities


def split_records(stream):
    payload = ''
    for line in stream:
        if line.strip() == "WARC/1.0":
            yield payload
            payload = ''
        else:
            payload += line

def withSpark(INPUT, OUTFILE, ES_DOMAIN, SPARQL_DOMAIN):
    from pyspark import SparkContext
    sc = SparkContext("yarn", "wdps1819")
    rdd = sc.newAPIHadoopFile(INPUT,
        "org.apache.hadoop.mapreduce.lib.input.TextInputFormat",
        "org.apache.hadoop.io.LongWritable",
        "org.apache.hadoop.io.Text",
        conf={"textinputformat.record.delimiter": "WARC/1.0"})

    rdd = rdd.flatMap(lambda j: find_labels(j, ES_DOMAIN, SPARQL_DOMAIN))
    rdd = rdd.saveAsTextFile(OUTFILE)

def withoutSpark(INPUT, ES_DOMAIN, SPARQL_DOMAIN):
    i = 0
    with open(INPUT, errors='ignore') as fo:
        for record in split_records(fo):
            # number of documents iteration
            #if i >= 10:
            #    break
            #i += 1

            find_labels(record, ES_DOMAIN, SPARQL_DOMAIN)


if __name__ == '__main__':
    import sys
    try:
        _, INFILE, OUTFILE, ES_DOMAIN, SPARQL_DOMAIN = sys.argv
    except Exception as e:
        print('Usage: python starter-code.py INPUT DOMAIN')
        sys.exit(0)

    #withSpark(INFILE, OUTFILE, ES_DOMAIN, SPARQL_DOMAIN)
    withoutSpark(INFILE, ES_DOMAIN, SPARQL_DOMAIN)
