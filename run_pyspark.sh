#!/usr/bin/env bash

YOUR_SPARK_HOME=/home/wdps1819/scratch/spark/bin/spark-submit

exec $YOUR_SPARK_HOME --master yarn $@
