import spacy
from pandas import DataFrame
import nltk.corpus

import sys
import os
import re

nlp = spacy.load("en_core_web_sm")

TAGS = ['ADJ', 'ADP', 'ADV', 'AUX', 'CONJ', 'CCONJ', 'DET', 'INTJ', 'NOUN', 'NUM', 'PART', 'PRON', 
        'PROPN', 'PUNCT', 'SCONJ', 'SYM', 'VERB', 'X', 'SPACE']

NER = {
    'specific' : ['PERSON', 'ORG', 'GPE', 'EVENT', 'WORK_OF_ART'],
    'general'  : ['NORP', 'FAC', 'LOC', 'PRODUCT', 'LAW', 'LANGUAGE'],
    'datetime' : ['DATE', 'TIME'],
    'numeric'  : ['PERCENT', 'MONEY', 'QUANTITY', 'ORDINAL', 'CARDINAL']
}

def get_features(s, p, o):
    # add features: count POS tag, and named entities.

    count_tags = []
    for t in TAGS:
        count_tags.append(
            len([j for j in p if j.pos_ == t])
        )
    
    assert len(count_tags) == len(TAGS)
    
    p_ner_count = []
    for _, l in NER.items():
        p_ner_count.append(
            len([j for j in p if j.ent_type_ in l])
        )
    
    assert len(p_ner_count) == len(NER)
    
    s_ner = []
    o_ner = []
    for _, j in NER.items():
        if s.ent_type_ in j:
            s_ner.append(1)
        else:
            s_ner.append(0)
            
        if o.ent_type_ in j:
            o_ner.append(1)
        else:
            o_ner.append(0)
    
    assert len(s_ner) == len(NER)
    assert len(o_ner) == len(NER)

    return [s, " ".join([x.string for x in p]), o, len(p), *count_tags, *p_ner_count, *s_ner, *o_ner]

def find_relations(payload):
    content = payload
    
    for sentence in content:
        doc = parse(sentence)
        triples = find_triples(doc)
        triples = remove_redundancy(triples)
        for s, p, o in triples:
            yield s, p, o 

def remove_redundancy(triples):
    for s, p, o in triples:
        p = [i for i in p if i.tag_ not in [',', ':', 'NFP']]
        if len(p) > 0:
            yield s, p, o

def find_triples(doc):
    triples = []

    for sent in doc.sents:    
        nnps = list(filter(lambda w: w.tag_ == 'NNP', sent))
        if len(nnps) < 2:
            continue
        
        current_relation = []
        current_nnp_i = 0
        current_found = False
        for w in sent:
            
            if w == nnps[current_nnp_i]:
                if len(current_relation) > 0:
                    triples.append( (nnps[current_nnp_i - 1], current_relation, nnps[current_nnp_i]) )
                    current_relation = []
                
                if current_nnp_i + 1 < len(nnps):
                    current_nnp_i = current_nnp_i + 1
                    current_found = True
                else:
                    break
            
            elif current_found:
                current_relation.append(w)
    return triples

def parse(content):
    doc = nlp(content)
    
    for span in list(doc.ents) + list(doc.noun_chunks):
        span.merge()
    return doc

def get_treebank_sentences():
    sentences = []
    for s in nltk.corpus.treebank.sents():
        # concat words to sentences
        sentence = " ".join(s)

        # eliminate unnecesary spaces
        sentence = sentence.replace(' ,', ',').replace(' .', '.').replace(' %', '%').replace('$ ', '$')
        sentences.append(sentence)
    return sentences

if __name__ == '__main__':
    try:
        _, OUTPUT = sys.argv
    except Exception as e:
        print('Usage: python3.5 RE_create_training_set.py OUTPUT')
        sys.exit(0)

    sentences = get_treebank_sentences()

    data = []
    for s, p, o in find_relations(sentences):
        data.append(get_features(s, p, o))

    p_NERs = ['p_'+k for k in NER.keys()]
    s_NERs = ['s_'+k for k in NER.keys()]
    o_NERs = ['o_'+k for k in NER.keys()]

    df = DataFrame(data, columns=['subject', 'predicate', 'object', 'total_tokens', *TAGS, *p_NERs, *s_NERs, *o_NERs])
    df['target'] = None
    df.to_csv(OUTPUT, index=False)
