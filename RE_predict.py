from bs4 import BeautifulSoup
import spacy
from pandas import DataFrame
import pickle

import os
import sys

KEYNAME = "WARC-TREC-ID" # or WARC-RECORD-ID ??
KEYHTML = "Content-Type"

nlp = spacy.load("en_core_web_sm")

TAGS = ['ADJ', 'ADP', 'ADV', 'AUX', 'CONJ', 'CCONJ', 'DET', 'INTJ', 'NOUN', 'NUM', 'PART', 'PRON', 
        'PROPN', 'PUNCT', 'SCONJ', 'SYM', 'VERB', 'X', 'SPACE']

NER = {
    'specific' : ['PERSON', 'ORG', 'GPE', 'EVENT', 'WORK_OF_ART'],
    'general'  : ['NORP', 'FAC', 'LOC', 'PRODUCT', 'LAW', 'LANGUAGE'],
    'datetime' : ['DATE', 'TIME'],
    'numeric'  : ['PERCENT', 'MONEY', 'QUANTITY', 'ORDINAL', 'CARDINAL']
}


def get_features(key, s, p, o):
    # add features: count POS tag, and named entities.

    count_tags = []
    for t in TAGS:
        count_tags.append(
            len([j for j in p if j.pos_ == t])
        )
    
    assert len(count_tags) == len(TAGS)
    
    p_ner_count = []
    for _, l in NER.items():
        p_ner_count.append(
            len([j for j in p if j.ent_type_ in l])
        )
    
    assert len(p_ner_count) == len(NER)
    
    s_ner = []
    o_ner = []
    for i, j in NER.items():
        if s.ent_type_ in j:
            s_ner.append(1)
        else:
            s_ner.append(0)
            
        if o.ent_type_ in j:
            o_ner.append(1)
        else:
            o_ner.append(0)
    
    assert len(s_ner) == len(NER)
    assert len(o_ner) == len(NER)

    return [key, s, " ".join([x.string for x in p]), o], [len(p), *count_tags, *p_ner_count, *s_ner, *o_ner]

def find_relations(payload):
    blocks = payload.split('\n\n', maxsplit=2)

    key = None
    for line in blocks[0].splitlines():
        if line.startswith(KEYNAME):
            key = line.split(': ')[1]

    content = None
    if len(blocks) == 3:
        content = parse_html(blocks[2])

    # eliminate parsed html pages without content
    if content is None:
        return key, []

    content = remove_blank_lines(content)
    content = remove_special_chars(content)
    doc = parse(content)

    triples = find_triples(doc)
    return key, triples

def find_triples(doc):
    triples = []

    for sent in doc.sents:    
        nnps = list(filter(lambda w: w.tag_ in ['NNP', 'NN'], sent))
        if len(nnps) < 2:
            continue
        
        current_relation = []
        current_nnp_i = 0
        current_found = False
        for w in sent:
            
            if w == nnps[current_nnp_i]:
                if len(current_relation) > 0:
                    if (len(nnps[current_nnp_i - 1]) < 30 and len(current_relation) < 10 and len(nnps[current_nnp_i]) < 30):
                        # eliminate big words / relations with too many words
                        triples.append( (nnps[current_nnp_i - 1], current_relation, nnps[current_nnp_i]) )
                        current_relation = []
                
                if current_nnp_i + 1 < len(nnps):
                    current_nnp_i = current_nnp_i + 1
                    current_found = True
                else:
                    break
            
            elif current_found:
                current_relation.append(w)
    return triples

def parse(content):
    doc = nlp(content)
    
    for span in list(doc.ents) + list(doc.noun_chunks):
        span.merge()
    return doc

def remove_blank_lines(content):
    content = os.linesep.join([s for s in content.splitlines() if s])
    content = " ".join(content.split())
    return content

def remove_special_chars(content):
    content = content.encode('ascii', errors='ignore').decode('utf-8')

    for i in ['\'\'', '&', '#', '^', '\"', '\"', '\\', '_', '+', '=', '|']:
        content = content.replace(i, '')

    clean = ''
    for word in content.split(' '):
        word = word.strip()
        if len(word) > 0 and len(word) <= 20:
            clean += ' ' + word
    return clean

def parse_html(raw_html):
    soup = BeautifulSoup(raw_html, 'html.parser')

    # eliminate <script><script/>
    for s in soup.find_all(['script', 'style', 'aside']):
        s.extract()

    raw_text = soup.get_text()
    return raw_text

def split_records(stream):
    payload = ''
    for line in stream:
        if line.strip() == "WARC/1.0":
            yield payload
            payload = ''
        else:
            payload += line

def load_model(path):
    with open(path, "rb") as f:
        model = pickle.load(f)
    return model

if __name__ == '__main__':
    try:
        _, INPUT, MODEL = sys.argv
    except Exception as e:
        print('Usage: python3.5 RE_predict.py INPUT MODEL')
        sys.exit(0)

    model = load_model(MODEL)

    i = 0
    with open(INPUT, errors='ignore') as fo:
        for record in split_records(fo):
            i += 1

            key, triples = find_relations(record)
            for s, p, o in triples:
                index, features = get_features(key, s, p, o)
                pred = model.predict([features])
                for i in range(len(pred)):
                    if pred[i] == 1:
                        print(index)
