# Web Data Processing Systems

- Course: Web Data Processing Systems
- Code: XM_40020
- Coordinator: J. Urbani
- University: VU Amsterdam

## Group19 members
- D.J.M. van Weerdenburg (2554298)
- A. Pison (2558524)
- L. Jasmontas (2631481)

## Run code
First of all our implementation is inside **scratch/wdps/webdataprocessingsystems** folder
To start process need to execute ```run_implementation.sh``` file. Before need to change Input and Output files manually inside ```run_implementation.sh``` **line 7** and **line 8**:
```
INFILE=${2:-"hdfs:///user/bbkruit/sample.warc.gz"}
OUTFILE=${3:-"predictions.tsv"}
```
to
```
INFILE=${2:-"hdfs:///new_file.gz"}
OUTFILE=${3:-"new_predictions.tsv"}
```
Also it is possible to change **line 37** which runs python program, need change $INFILE and $OUTFILE values in this line:
```
time python3.5 $SCRIPT <(hdfs dfs -cat $INFILE | zcat) > $OUTFILE $OUTFILE $ES_NODE:$ES_PORT $KB_NODE:$KB_PORT
(second $OUTFILE is just for having same amount of parameters as for local implementation without testing)
```

## Implementation details
### Preprocessing
A WARC file consists of many records which are separated by two white spaces.  Each record consists of at most three blocks, where the third block contains HTML-code of a certain webpage.  So we first need to check if a record has such a third block.  If it does, we use the BeautifulSoup library to eliminate script and style blocks and to convert the HTML-code to actual text.  Also we are removing empty lines and multiple spaces.
Now that we have the actual text at our disposal, we want to do some NLP preprocessing on it.  We have decided to use spaCy for this task, as they claim to be the fastest NLP processor out there, meaning it will handle large inputs very well.  They also claim to have an accuracy which lies within 1% of the best available.
After  running  spaCy  we  end  up  with  a  lot  of  (named)  entity  mentions.   After  investigating  those,  we concluded there are quite a few named entities which do not make sense at all.  For instance, we encountered many named entities which are only a number like ‘1’ or ‘2’.  To improve accuracy and speed up the next phases, we have decided to filter out those weird named entities already. We are filtering them by their string structure: a mention cannot be longer than 20 symbols, should not start with a number and should not contain special symbols (!, #, ?, etc.)

### Entity linking
Our entity linking pipeline consists of the following phases:

1. Candidate entity generation.   To  generate  some  candidates  for  a  specific  mention  we  found  in  our text,  we  simply  query  Elasticsearch.   We  were  querying  Elasticsearch  with  the  ‘AND’  operation.   This means that if a mention consists of multiple words, then all these words must appear in the candidate entities.   This  improved  results  noticeable.   Elasticsearch  returns  candidates  with  corresponding  score (which reflects popularity of the candidate) and Freebase ID. We are picking only the top result because, after investigating a small subset of WARC records, picking the top ranked candidate from Elasticsearch gives better results than picking only the k ranked candidate, k > 1.
2. Candidate entity ranking. After the candidate generation phase, we are querying SPARQL using the Freebase ID of our candidates to get some information about the candidate entity.  We are comparing how many results SPARQL returns with ‘sameAs’ predicate.  If it less than 5 we considered that this is entity is not popular.  Also we could have for example run a string similarity measure (e.g.  dice score, Jaccard similarity) between the candidate entity and the mention.  Unfortunately, we were not able to implement SPARQL as we would like so we were considering just top candidate.
3. Unlinkable mention prediction.  We are limiting Elasticsearch to return only one candidate,  but it returns also property total which indicates the maximum number of candidates Elasticsearch could return for this query.  We check if total ≤ 3.  If so, we consider the mention as unlinkable.

### Performance
Our performance was improving slightly after specific changes, which is shown in table below.
**Implementation 1** consists of Elasticsearch ‘AND’ operation and filtering entities by their type.  Only geographical, organization and person entities are considered. **Implementation 2** consists of filtering mentions by their string structure:  a mention cannot be longer than 20 symbols, should not start with a number and should not contain special symbols (!, #, ?, etc.). **Implementation 3** is our final result.  Here, we added checks on total results from Elasticsearch and also total results on SPARQL ‘sameAs’ predicate.  We were also trying other SPARQL queries but they did not produce better results.  In our final implementation, it takes approximately 4.6 seconds to disambiguate one page.

|  metric | implementation 1  |  implementation 2 | implementation 3  |
| --------| --------| --------| --------|
| gold | 560 | 560 | 560 |
| predicted | 4760 | 3523 | 1353 |
| correct | 173 | 170 | 122 |
| precision | 0.03634 | 0.04825 | 0.09016 |
| recall | 0.30892 | 0.30357 | 0.21785 |
| **F1** | **0.06503** | **0.08327** | **0.12754** |

## Relation
For Relation Extraction we used the idea of TextRunner in a Traditional Extraction fashion.


For **creating the training set**, please run the following command:
```
python3.5 RE_create_training_set.py {output_file_training_set.csv}
```

The training set will contain the following features besides the KEYNAME, triple (subject, predicate, object) and target columns: total number of tokens in predicate, count all POS tags in predicate, count named entity types (see below) in predicate, the named entity type (see below) of the subject and the named entity type (see below) of the object.

Named entity types, classified by spaCy and mapped to 'specific', 'general', 'datetime' or 'numeric':
```
NER = {
    'specific' : ['PERSON', 'ORG', 'GPE', 'EVENT', 'WORK_OF_ART'],
    'general'  : ['NORP', 'FAC', 'LOC', 'PRODUCT', 'LAW', 'LANGUAGE'],
    'datetime' : ['DATE', 'TIME'],
    'numeric'  : ['PERCENT', 'MONEY', 'QUANTITY', 'ORDINAL', 'CARDINAL']
}
```

 [s, " ".join([x.string for x in p]), o, len(p), *count_tags, *p_ner_count, *s_ner, *o_ner]

The training set needs to be manually annotated. The output of the previous command is a CSV, which can easily be opened in a program like Excel. Please, provide only 0 or 1 in the target column when the relation is respectively negative or positive. We already did this for `RE_train.csv`.

When we have the annotations, we can **train the model**:
```
python3.5 RE_train_model.py {input_file_annotated_training_set.csv} {output_file_model.pickle}
```

This will be very quick, because our model is fast, plus we provided only a small dataset.

Now we are ready to **extract relations**. First, the webpages are cleaned by using bs4 and spaCy, as mentioned in previous section about Entity Linking. After that, noun phrases are taken from each sentence. All possible relations of the form <NNP> < text in between > <NNP> will be extracted. The first NNP will be the subject, the text in between the predicate and the last NNP the object. In this way triples are generated. From these triples the same features can be taken as in creating the training set.

```
python3.5 RE_predict.py {input_file_webpages} {input_file_model.pickle} > {output_file_predictions.txt}
```
for example: input_file_webpages = `sample.warc`

This line resulted in the output: `RE_predictions.txt`.

## Future work

We tried to implement our entity linking and relation extraction so, that they are both fast and accurate. However, there are still a lot of improvements possible. For instance, running our implementations on top of Spark will speed up things a lot. However, due to lasting issues with the DAS-4 cluster, we have decided to write just single Python programs to do the job. Also, as mentioned before, using SPARQL to gain more information about candidate entities would improve accuracy a lot.
