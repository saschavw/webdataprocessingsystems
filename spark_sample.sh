#!/usr/bin/env bash

# Usage: bash spark_sample.sh [INFILE] [OUTFILE]
SCRIPT=${1:-"starter-code-spark.py"}
INFILE=${2:-"hdfs:///user/wdps1819/sample.warc.gz"}
OUTFILE=${3:-"test_output"}

hdfs dfs -rm -r $OUTFILE

PYSPARK_PYTHON=$(readlink -f $(which python)) ~/scratch/spark/bin/spark-submit \
--master yarn $SCRIPT $INFILE $OUTFILE

hdfs dfs -cat $OUTFILE"/*" > $OUTFILE