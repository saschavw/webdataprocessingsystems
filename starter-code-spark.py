from pyspark import SparkContext
# from pyspark.sql import SparkSession
import sys
import collections

KEYNAME = "WARC-TREC-ID"

def find_google(record):
    # finds google
    _, payload = record
    key = None
    for line in payload.splitlines():
        if line.startswith(KEYNAME):
            key = line.split(': ')[1]
            break
    if key: # and ('Google' in payload):
        yield key + '\t' + 'Google' + '\t' + '/m/045c7b'

def f(x): 
    _, payload = x
    print(payload)

if __name__ == '__main__':
    try:
        _, INFILE, OUTFILE = sys.argv
    except Exception as e:
        print('Usage: python starter-code.py INFILE OUTFILE')
        sys.exit(0)

    sc = SparkContext("yarn", "wdps1819")

    rdd = sc.newAPIHadoopFile(INFILE,
        "org.apache.hadoop.mapreduce.lib.input.TextInputFormat",
        "org.apache.hadoop.io.LongWritable",
        "org.apache.hadoop.io.Text",
        conf={"textinputformat.record.delimiter": "WARC/1.0"})

    print(rdd.count())
    rdd = rdd.flatMap(find_google)
    print(rdd.count())
    rdd = rdd.saveAsTextFile(OUTFILE)
