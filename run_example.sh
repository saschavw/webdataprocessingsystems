# Files
SCRIPT=${1:-"starter-code.py"}
SCORE=${2:-"score.py"}
ANNOTATIONS=${3:-"data/sample.annotations.tsv"}
INFILE=${4:-"sample.warc"}
SPARK_INFILE=${5:-"hdfs:///user/bbkruit/sample.warc.gz"}
OUTFILE=${6:-"predictions.tsv"}

# SPARQL
KB_PORT=9090
KB_BIN=/home/bbkruit/scratch/trident/build/trident
KB_PATH=/home/jurbani/data/motherkb-trident

prun -o .kb_log -v -np 1 $KB_BIN server -i $KB_PATH --port $KB_PORT </dev/null 2> .kb_node &
echo "waiting 5 seconds for trident to set up..."
until [ -n "$KB_NODE" ]; do KB_NODE=$(cat .kb_node | grep '^:' | grep -oP '(node...)'); done
sleep 5
KB_PID=$!
echo "trident should be running now on node $KB_NODE:$KB_PORT (connected to process $KB_PID)"


# ElasticSearch
ES_PORT=9200
ES_BIN=$(realpath ~/scratch/elasticsearch-2.4.1/bin/elasticsearch)

>.es_log*
prun -o .es_log -v -np 1 ESPORT=$ES_PORT $ES_BIN </dev/null 2> .es_node &
echo "waiting for elasticsearch to set up..."
until [ -n "$ES_NODE" ]; do ES_NODE=$(cat .es_node | grep '^:' | grep -oP '(node...)'); done
ES_PID=$!
until [ -n "$(cat .es_log* | grep YELLOW)" ]; do sleep 1; done
echo "elasticsearch should be running now on node $ES_NODE:$ES_PORT (connected to process $ES_PID)"

### Without spark
# Test running
time python3.5 $SCRIPT $SCRIPT <(hdfs dfs -cat $INFILE | zcat) > <(hdfs dfs > $TESTOUTFILE) $OUTFILE $ES_NODE:$ES_PORT $KB_NODE:$KB_PORT
#python3.5 $SCORE $ANNOTATIONS $OUTFILE
# Regular running
time python3.5 $SCRIPT $INFILE $OUTFILE $ES_NODE:$ES_PORT $KB_NODE:$KB_PORT

### With spark
#$ source nltk_env/bin/activate
#virtualenv --relocatable nltk_env
#zip -r nltk_env.zip nltk_env

#PYSPARK_PYTHON=$(readlink -f $(which python)) /home/wdps1819/scratch/spark/bin/spark-submit \
#--conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=./NLTK/nltk_env/bin/python \
#--master yarn-cluster \
#--archives nltk_env.zip#NLTK,tokenizers.zip#tokenizers,taggers.zip#taggers \
#$SCRIPT $INFILE $OUTFILE $ES_NODE:$ES_PORT $KB_NODE:$KB_PORT
#hdfs dfs -cat $OUTFILE"/*" > $OUTFILE

kill $ES_PID
kill $KB_PID
